package com.gigactic.decodeeditencode.editor;

public interface EncodingProgressListener {
    public void onFinished(String outputPath, long duration);
    public void onProgress(int percent);
    public void onError();
}
