package com.gigactic.decodeeditencode.editor.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.gigactic.decodeeditencode.R;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class ResourceLoader {
    private static Context context;
    public static void init(Context context){
       ResourceLoader.context = context;
    }
    public static String load(int id){
        try {
            String result = IOUtils.toString(context.getResources().openRawResource(id), Charsets.UTF_8);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        return null;
        }
    }

    public static Bitmap getTextureBitmap() {
        BitmapFactory.Options a =  new BitmapFactory.Options();
        a.inScaled = false;
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.bg,a);
    }
}
