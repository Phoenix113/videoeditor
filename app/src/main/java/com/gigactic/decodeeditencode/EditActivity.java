package com.gigactic.decodeeditencode;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.gigactic.decodeeditencode.editor.EncodingProgressListener;
import com.gigactic.decodeeditencode.editor.DecoderEncoder;
import com.gigactic.decodeeditencode.editor.VideoSurfaceView;

import com.gigactic.decodeeditencode.editor.utils.ResourceLoader;

import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class EditActivity extends AppCompatActivity {


    private EncodingProgressListener encodingListener;
    private VideoView videoView;
    private String selectedVideoPath;

    private VideoSurfaceView glVideoView = null;
    private MediaPlayer mMediaPlayer = null;
    private RelativeLayout containerView;

    private Executor executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                init();
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        } else {
            init();
        }
    }

    private void init() {
        ResourceLoader.init(getApplicationContext());
        setContentView(R.layout.activity_edit);
        videoView = findViewById(R.id.videoView);
        containerView = findViewById(R.id.container);
        encodingListener = new EncodingProgressListener() {
            @Override
            public void onFinished(final String outputPath, final long duration) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Everything took (no audio): " + (duration / 1000.f) + " seconds", Toast.LENGTH_LONG).show();
                        videoView.setVideoPath(outputPath);
                        videoView.setVisibility(View.VISIBLE);
                        findViewById(R.id.progressbar).setVisibility(View.INVISIBLE);
                        containerView.removeAllViews();
                        Toast.makeText(getApplicationContext(), "Saved to sdcard", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onProgress(final int percent) {
            }

            @Override
            public void onError() {
                //Do nothing yet
            }
        };

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });

        executor = Executors.newSingleThreadExecutor();
    }

    public void encode(final View view) {
        if (selectedVideoPath == null) {
            Toast.makeText(getApplicationContext(), "Please select a video before", Toast.LENGTH_SHORT).show();
            return;
        }
        if (videoView.isPlaying())
            videoView.stopPlayback();
        Toast.makeText(getApplicationContext(), "Encoding current state", Toast.LENGTH_LONG).show();
        mMediaPlayer.stop();
        findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        executor.execute(new EncodingRunnable((selectedVideoPath)));
        containerView.removeAllViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView != null) videoView.stopPlayback();
        if (mMediaPlayer != null) mMediaPlayer.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (glVideoView != null) {
            glVideoView.onResume();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            init();
        } else {
            finish();
        }
    }

    public void openPicker(View view) {
        new VideoPicker.Builder(this)
                .mode(VideoPicker.Mode.GALLERY)
                .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
            selectedVideoPath = mPaths.get(0);
            if (selectedVideoPath == null) {
                finish();
            }
            initGLvid();
            ((TextView) findViewById(R.id.textWarning)).setText(FilenameUtils.getName(selectedVideoPath) + " selected");
        }
    }

    private void initGLvid() {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(selectedVideoPath);
        } catch (Exception e) {
        }
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(selectedVideoPath);
        String height = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String width = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        String orientation = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Point outVidDims = null;
        if (Integer.parseInt(orientation) / 90 > 0) {
            outVidDims = new Point(Integer.parseInt(height), Integer.parseInt(width));
        } else {
            outVidDims = new Point(Integer.parseInt(width), Integer.parseInt(height));
        }
        glVideoView = new VideoSurfaceView(this, mMediaPlayer, outVidDims);
        glVideoView.setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                glVideoView.redraw();
            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        containerView.removeAllViews();
        containerView.addView(glVideoView);
        Toast.makeText(getApplicationContext(), "You can move the video, and encode it", Toast.LENGTH_LONG).show();
    }

    public String getPath(Uri uri) {
        if (Build.VERSION.SDK_INT > 25) {
            File file = new File(uri.getPath());//create path from uri
            final String[] split = file.getPath().split(":");//split the path.
            return split[1];//assign it to a string(your choice).
        } else {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(this, uri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
    }

    public void reset(View view) {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private class EncodingRunnable implements Runnable {
        private final DecoderEncoder decoderEncoder = new DecoderEncoder();
        private String videoPath = null;

        public EncodingRunnable(String videoPath) {
            this.videoPath = videoPath;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
            try {
                if (videoPath != null)
                    decoderEncoder.decodeEditMux(getApplicationContext(), encodingListener, new File(videoPath), glVideoView.getSize());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }
}
